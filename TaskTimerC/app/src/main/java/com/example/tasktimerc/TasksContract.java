package com.example.tasktimerc;
import static com.example.tasktimerc.AppProvider.CONTENT_AUTHORITY;
import static com.example.tasktimerc.AppProvider.CONTENT_AUTHORITY_URI;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by timbuchalka on 18/10/16.
 */

public class TasksContract {
    static final String TABLE_NAME = "Tasks";
    /**
     * The URI to access the Tasks table.
     * CONTENT_AUTHORITY_URI comes from AppProvider class.
     */
    public static final Uri CONTENT_URI = Uri.withAppendedPath(CONTENT_AUTHORITY_URI, TABLE_NAME);
    static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + CONTENT_AUTHORITY + "." + TABLE_NAME;
    static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd." + CONTENT_AUTHORITY + "." + TABLE_NAME;

    // Tasks fields
    public static class Columns {
        public static final String _ID = BaseColumns._ID;
        public static final String TASKS_NAME = "Name";
        public static final String TASKS_DESCRIPTION = "Description";
        public static final String TASKS_SORTORDER = "SortOrder";

        //private constructor.
        private Columns() {
            // private constructor to prevent instantiation.
        }//end of Columns class
    }

    public static Uri buildTaskUri(long taskId) {
        return ContentUris.withAppendedId(CONTENT_URI, taskId);
    }

    public static long getTaskId(Uri uri) {
        return ContentUris.parseId(uri);
    }
}
