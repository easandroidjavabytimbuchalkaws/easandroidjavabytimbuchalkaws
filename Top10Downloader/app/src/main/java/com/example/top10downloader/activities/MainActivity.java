package com.example.top10downloader.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.top10downloader.R;
import com.example.top10downloader.adapters.FeedAdapter;
import com.example.top10downloader.models.FeedEntry;
import com.example.top10downloader.utilities.ParseApplications;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private ListView listApps;
    private String feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/limit=%d/xml";
    private int feedLimit = 10;
    private String feedTitle = "";
    private String currentTitlePart2;
    private String feedTitle1 = "Top 10 ";
    private String feedTitle2 = "Top 25 ";
    private String feedTitle3 = "free apps";
    private String feedTitle4 = "paid apps";
    private String feedTitle5 = "songs";

    //variables to save the current feedLimit.
    // before activity is destroyed.
    //private static final String STATE_FEEDLIMIT = "CurrentFeedLimit"; I tried it
    private String feedCacheUrl = "INVALIDATED";
    public static final String STATE_URL = "feedUrl";
    public static final String STATE_LIMIT = "feedLimit";
    public static final String STATE_TITLE = "feedTitle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listApps = findViewById(R.id.xmlListView);

        /*if saveInstanceState != null means that the activity was restarted
        because a rotation or something else.
         */
        if(savedInstanceState != null){
            feedUrl = savedInstanceState.getString(STATE_URL);
            feedLimit = savedInstanceState.getInt(STATE_LIMIT);
            feedTitle = savedInstanceState.getString(STATE_TITLE);
            this.setTitle(feedTitle);
        }
        //downloadUrl("http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/limit=10/xml");
        downloadUrl(String.format(feedUrl, feedLimit));
    }

    /*@Override I created this two methods. They weren't implemented in tom's solution.
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        //outState.putInt(STATE_FEEDLIMIT, feedLimit);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        feedUrl = savedInstanceState.getString(STATE_URL);
        //feedLimit = savedInstanceState.getInt(STATE_FEEDLIMIT);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.feeds_menu, menu);
        if(feedLimit == 10) {
            menu.findItem(R.id.mnu10).setChecked(true);
        } else {
            menu.findItem(R.id.mnu25).setChecked(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        //String feedUrl;
        switch(id) {
            case R.id.mnuFree:
                feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/limit=%d/xml";
                if(feedLimit == 10) {
                    feedTitle = feedTitle1 + feedTitle3;
                } else {
                    feedTitle = feedTitle2 + feedTitle3;
                }
                currentTitlePart2 = feedTitle3;
                break;
            case R.id.mnuPaid:
                feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/toppaidapplications/limit=%d/xml";
                if(feedLimit == 10) {
                    feedTitle = feedTitle1 + feedTitle4;
                } else {
                    feedTitle = feedTitle2 + feedTitle4;
                }
                currentTitlePart2 = feedTitle4;
                break;
            case R.id.mnuSongs:
                feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topsongs/limit=%d/xml";
                if(feedLimit == 10) {
                    feedTitle = feedTitle1 + feedTitle5;
                } else {
                    feedTitle = feedTitle2 + feedTitle5;
                }
                currentTitlePart2 = feedTitle5;
                break;
            case R.id.mnu10:
            case R.id.mnu25:
                if(!item.isChecked()){
                    item.setChecked(true);
                    feedLimit = 35 - feedLimit;
                    Log.d(TAG, "onOptionsItemSelected: " + item.getTitle() + " setting feedLimit to " + feedLimit);
                }else {
                    Log.d(TAG, "onOptionsItemSelected: " + item.getTitle() + " feedLimit unchanged");
                }
                if(feedLimit == 10) {
                    feedTitle = feedTitle1 + currentTitlePart2;
                } else {
                    feedTitle = feedTitle2 + currentTitlePart2;
                }
                break;
            case R.id.mnuRefresh:
                feedCacheUrl = "INVALIDATED";
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        this.setTitle(feedTitle);
        //downloadUrl(feedUrl);
        downloadUrl(String.format(feedUrl, feedLimit));
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_URL, feedUrl);
        outState.putInt(STATE_LIMIT, feedLimit);
        outState.putString(STATE_TITLE, feedTitle);
        super.onSaveInstanceState(outState);
    }

    private void downloadUrl(String feedUrl){
        if(!feedUrl.equalsIgnoreCase(feedCacheUrl)){
            Log.d(TAG, "downloadUrl: starting AsyncTask");
            DownloadData downloadData = new DownloadData();
            downloadData.execute(feedUrl);
            feedCacheUrl = feedUrl;
            Log.d(TAG, "downloadUrl: done"); 
        } else {
            Log.d(TAG, "downloadUrl: URL not changed");
        }
        
    }

    //internal class to handle the async functionality.
    private class DownloadData extends AsyncTask<String, Void, String> {
        private static final String TAG = "DownloadData";

        /*this method is executed once the
        doInBackground() is done
         */
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Log.d(TAG, "onPostExecute: paramete is " + s);
            ParseApplications parseApplications = new ParseApplications();
            parseApplications.parse(s);

//            default adapter.
//            ArrayAdapter<FeedEntry> arrayAdapter = new ArrayAdapter<FeedEntry>(
//                    MainActivity.this, R.layout.list_item, parseApplications.getApplications());
//            listApps.setAdapter(arrayAdapter);
            FeedAdapter<FeedEntry> myAdapter = new FeedAdapter<>(MainActivity.this, R.layout.list_record,
                    parseApplications.getApplications());
            listApps.setAdapter(myAdapter);
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.d(TAG, "doInBackground: starts with " + strings[0]);
            String rssFeed = downloadXML(strings[0]);
            if (rssFeed == null) {
                Log.e(TAG, "doInBackground: Error downloading");
            }
            return rssFeed;
        }

        private String downloadXML(String urlPath) {
            StringBuilder xmlResult = new StringBuilder();

            try {
                URL url = new URL(urlPath);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int response = connection.getResponseCode();
                Log.d(TAG, "downloadXML: The response code was " + response);
//                InputStream inputStream = connection.getInputStream();
//                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                BufferedReader reader = new BufferedReader(inputStreamReader);
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(
                                connection.getInputStream()));
                int charsRead;
                char[] inputBuffer = new char[500];
                while (true) {
                    charsRead = reader.read(inputBuffer);
                    if (charsRead < 0) {
                        break;
                    }
                    if (charsRead > 0) {
                        xmlResult.append(String.copyValueOf(inputBuffer, 0, charsRead));
                    }
                }
                reader.close();
                return xmlResult.toString();
            } catch (MalformedURLException e) {
                Log.e(TAG, "downloadXML: Invalid URL " + e.getMessage());
            } catch (IOException e) {
                Log.e(TAG, "downloadXML: IO Exception reading data " + e.getMessage());
            } catch (SecurityException e) {
                Log.e(TAG, "downloadXML: Security Exception. Needs permission? " + e.getMessage());
//                e.printStackTrace();
            }
            return null;
        }
    }
}