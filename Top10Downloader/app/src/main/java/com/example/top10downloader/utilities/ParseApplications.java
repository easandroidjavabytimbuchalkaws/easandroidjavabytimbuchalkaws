package com.example.top10downloader.utilities;

import android.util.Log;

import com.example.top10downloader.models.FeedEntry;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

public class ParseApplications {
    private static final String TAG = "ParseApplications";
    private ArrayList<FeedEntry> applications;

    //constructor
    public ParseApplications() {
        this.applications = new ArrayList();
    }

    public ArrayList<FeedEntry> getApplications() {
        return applications;
    }

    public boolean parse(String xmlData){
        boolean status = true; //any exception turns false this variable.
        FeedEntry currentRecord = null; //stores a new entry tag info.
        boolean inEntry = false; //detects if the tag belongs to the entry tag.
        String textValue = ""; //stores the current tag content.

        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new StringReader(xmlData));
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT){
                String tagName = xpp.getName();
                switch (eventType){
                    case XmlPullParser.START_TAG:
                        //Log.d(TAG, "parse: Starting tag for " + tagName);
                        if("entry".equalsIgnoreCase(tagName)){
                            inEntry = true;
                            currentRecord = new FeedEntry();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        textValue = xpp.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        //Log.d(TAG, "parse: Ending tag for " + tagName);
                        if(inEntry){
                            if("entry".equalsIgnoreCase(tagName)){
                                applications.add(currentRecord);
                                inEntry = false;
                            }else if("name".equalsIgnoreCase(tagName)){
                                currentRecord.setName(textValue);
                            }else if("artist".equalsIgnoreCase(tagName)){
                                currentRecord.setArtist(textValue);
                            }else if("releaseDate".equalsIgnoreCase(tagName)){
                                currentRecord.setReleaseDate(textValue);
                            }else if("summary".equalsIgnoreCase(tagName)){
                                currentRecord.setSummary(textValue);
                            }else if("image".equalsIgnoreCase(tagName)){
                                currentRecord.setImageURL(textValue);
                            }
                        }
                        break;

                    default:
                        //nothing else to do.
                }
                eventType = xpp.next();
            }
//            for(FeedEntry app: applications){
//                Log.d(TAG, "*********************");
//                Log.d(TAG, app.toString());
//            }
        } catch (Exception e){
            status = false;
            e.printStackTrace();
        }

        return status;
    }
}
