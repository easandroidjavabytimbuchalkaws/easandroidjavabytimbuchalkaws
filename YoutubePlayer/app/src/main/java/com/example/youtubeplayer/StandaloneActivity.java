package com.example.youtubeplayer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.youtube.player.YouTubeStandalonePlayer;

public class StandaloneActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standalone);

        Button btnPlayVideo = findViewById(R.id.btnPlayVideo);
        Button btnPlayList = findViewById(R.id.btnPlayList);

        //this is one way used to assign the same listener to multiple buttons.
        btnPlayVideo.setOnClickListener(this);
        btnPlayList.setOnClickListener(this);

//        /*creating our own listener.
//            it creates an onClickListener object (ourListener).
//         */
//        View.OnClickListener ourListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        };
//
//        //this is another way used to assign the same listener to multiple buttons.
//        btnPlayVideo.setOnClickListener(ourListener);
//        btnPlayList.setOnClickListener(ourListener);
//
//        //this way is typical used for a single button.
          //it's necessary to repeat the same code for each button.
//        btnPlayVideo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//        btnPlayList.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;

        switch (view.getId()) {
            case R.id.btnPlayVideo:
                intent = YouTubeStandalonePlayer.createVideoIntent(this,
                                                                    YoutubeActivity.GOOGLE_API_KEY,
                                                                    YoutubeActivity.YOUTUBE_VIDEO_ID,
                                                                    0, true, false);
                break;

            case R.id.btnPlayList:
                intent = YouTubeStandalonePlayer.createPlaylistIntent(this,
                                                                        YoutubeActivity.GOOGLE_API_KEY,
                                                                        YoutubeActivity.YOUTUBE_PLAYLIST,
                                                                        0, 0, true, true);
                /*next 5 lines of code implement a search
                 */
                /*Intent intent2 = new Intent(Intent.ACTION_SEARCH);
                intent2.setPackage("com.google.android.youtube");
                intent2.putExtra("query", "007hernyvideos");
                intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent2);*/
                break;

            default:
        }

        if(intent != null){
            startActivity(intent);
        }
    }
}
