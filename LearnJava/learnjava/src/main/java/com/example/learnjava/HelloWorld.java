package com.example.learnjava;

public class HelloWorld {
    public static String tim;
    public static int timSalary = 32;
    public static int monthly = timSalary * 4;
    public static int timWeeklySalary = 32;
    public static int timsMonthlySalary = timWeeklySalary * 4;
    public static int apples = 6;
    public static int oranges = 5;
    public static int weeks = 130;

    public static void main(String[] args) {
        System.out.println("Hello World!!");
        System.out.println("My first Java program");

        tim = "Tim Buchalka";
        System.out.println(tim);

//        System.out.println(timSalary);
//        System.out.println(monthly);

        //lesson 54
        System.out.println("Weekly salary: " + timWeeklySalary);
        System.out.println("Monthly salary: " + timsMonthlySalary);

        int fruit = apples - oranges;
        System.out.println("I have " + fruit + " fruit");

        double years = weeks / 52.0;
        System.out.println(weeks + " weeks is " + years + " years");

        System.out.println("Hello, " + tim + " here");
    }
}