package com.example.flickrbrowserd;

import android.util.Log;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

/* this class will be an utility to be used by: MainActivity,
SearchActivity, and PhotoDetailActivity classes.
these classes will share the AppCompatActivity class methods,
and the activateToolbar() of this class.
 */
public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    static final String FLICKR_QUERY = "FLICKR_QUERY";
    static final String PHOTO_TRANSFER = "PHOTO_TRANSFER";

    /*this method renders the toolBar, and it will allow
     an activity to choose whether the toolbar should have
     the home button (<--)showing or not.
     */
    void activateToolbar(boolean enableHome) {
        Log.d(TAG, "activateToolbar: starts");
        ActionBar actionBar = getSupportActionBar();
        if(actionBar == null) {
            Toolbar toolbar = findViewById(R.id.toolbar);
            if(toolbar != null) {
                setSupportActionBar(toolbar);
                actionBar = getSupportActionBar();
            }
        }

        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(enableHome);
        }
    }
}
