package com.example.flickrbrowserd;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

//the FlickrImageViewHolder is defined as inner class with static attribute at the end of the code.
class FlickrRecyclerViewAdapter extends RecyclerView.Adapter<FlickrRecyclerViewAdapter.FlickrImageViewHolder>{
    private static final String TAG = "FlickrRecyclerViewAdapt";
    private List<Photo> mPhotosList;
    private Context mContext;

    //constructor.
    FlickrRecyclerViewAdapter(Context context, List<Photo> photosList) {
        mContext = context;
        mPhotosList = photosList;
    }

    @NonNull
    @Override
    public FlickrImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        /* Called by the layout manager when it needs a new view.
        Every photo object causes to execute this method.
        as a matter of fact getItemCount(), onCreateViewHolder(), and onBindViewHolder()
        are called in this order.
        it constructs an empty view according to the browse layout.
         */
        Log.d(TAG, "onCreateViewHolder: new view requested");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browse, parent, false);
        return new FlickrImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FlickrImageViewHolder holder, int position) {
        /* Called by the layout manager when it wants new data in an existing row.
        this fills with data the empty view created in onCreateViewHolder().
         */
        if((mPhotosList == null) || (mPhotosList.size() == 0)) {
            holder.thumbnail.setImageResource(R.drawable.placeholder);
            holder.title.setText(R.string.empty_photo);
        } else {
            Photo photoItem = mPhotosList.get(position);
            Log.d(TAG, "onBindViewHolder: " + photoItem.getTitle() + " --> " + position);
            //picasso runs asynchronously.
            Picasso.get().load(photoItem.getImage())
                    .error(R.drawable.ic_launcher_background)
                    .placeholder(R.drawable.placeholder)
                    .into(holder.thumbnail,new Callback() {
                        /*callBack really starts an anonymous function to
                        allow debugging inside of picasso. It has 2 methods:
                        onSuccess() and onError().
                         */
                        @Override
                        public void onSuccess() {
                            //do nothing.
                        }

                        @Override
                        public void onError(Exception e) {
                            if (e.getCause() != null) {
//                            Toast.makeText(mContext, e.getCause().toString(), Toast.LENGTH_LONG).show();
                                Log.e(TAG, "picasso: Error cause: " + e.getCause().toString() + " " + photoItem.getImage());
                            }else {
                                Log.e(TAG, "picasso: Error message: "  + e.getMessage() + " " + photoItem.getImage());
                            }
                        }
                    });

            holder.title.setText(photoItem.getTitle());
        }
        Log.d(TAG, "onBindViewHolder: ends.");
    }

    @Override
    public int getItemCount() {
//        Log.d(TAG, "getItemCount: called");
        return ((mPhotosList != null) && (mPhotosList.size() !=0) ? mPhotosList.size() : 1);
    }

    void loadNewData(List<Photo> newPhotos) {
        //this method is called by onDataAvailable().
        mPhotosList = newPhotos;
        notifyDataSetChanged();
    }

    //this is executed to render the selected photo from the list.
    public Photo getPhoto(int position) {
        return ((mPhotosList != null) && (mPhotosList.size() !=0) ? mPhotosList.get(position) : null);
    }

    //inner class for our viewHolder.
    static class FlickrImageViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = "FlickrImageViewHolder";
        ImageView thumbnail = null;
        TextView title = null;

        //constructor.
        public FlickrImageViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "FlickrImageViewHolder: starts");
            this.thumbnail = itemView.findViewById(R.id.thumbnail);
            this.title = itemView.findViewById(R.id.title);
        }
    }
}
