package com.example.flickrbrowserc;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.List;

public class MainActivity extends AppCompatActivity implements GetFlickrJsonData.OnDataAvailable {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* the GetRawData is a class that runs the asynchronic
        background job.
        it's not more called in here because the GetFlickrJsonData implementation.
         */
//        GetRawData getRawData = new GetRawData(this);
//        getRawData.execute("https://api.flickr.com/services/feeds/photos_public.gne?tags=android,nougat,sdk&tagmode=any&format=json&nojsoncallback=1");

        Log.d(TAG, "onCreate: ends");
    }

    @Override
    protected void onResume() {
        /*for now this code is not impacted if it were inside the
        onCreate()
         */
        Log.d(TAG, "onResume starts");
        super.onResume();
        GetFlickrJsonData getFlickrJsonData = new GetFlickrJsonData(this, "https://api.flickr.com/services/feeds/photos_public.gne", "en-us", true);
//        getFlickrJsonData.executeOnSameThread("android, nougat");
        //now the getFlickrJsonData runs async.
        getFlickrJsonData.execute("android,nougat");
        Log.d(TAG, "onResume ends");

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        Log.d(TAG, "onCreateOptionsMenu() returned: " + true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        Log.d(TAG, "onOptionsItemSelected() returned: returned");
        return super.onOptionsItemSelected(item);
    }

    //this method was replaced by the onDataAvailable().
//    @Override
//    public void onDownloadComplete(String data, DownloadStatus status) {
//        /*this method is executed from the GetRawData
//        and gets the response content and its status.
//         */
//        if(status == DownloadStatus.OK) {
//            Log.d(TAG, "onDownloadComplete: data is " + data);
//        } else {
//            // download or processing failed
//            Log.e(TAG, "onDownloadComplete failed with status " + status);
//        }
//    }

    @Override
    public void onDataAvailable(List<Photo> data, DownloadStatus status) {
        /*this method is executed from the GetFlickrJsonData
        and brings the photo objects and the response status.
         */
        if(status == DownloadStatus.OK) {
            Log.d(TAG, "onDataAvailable: data is " + data);
        } else {
            // download or processing failed
            Log.e(TAG, "onDataAvailable failed with status " + status);
        }
    }
}
